<?php

declare(strict_types = 1);

namespace Tests;

use App\Calculator;
use PHPUnit\Framework\TestCase;

final class CalculatorTest extends TestCase
{

    /**
     * @covers \App\Calculator
     */
    public function testPlusFunction(): void
    {
        $this->assertSame(
            5,
            Calculator::plusFunction(2, 3)
        );
    }
}
