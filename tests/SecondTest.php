<?php

declare(strict_types = 1);

namespace Tests;

use PHPUnit\Framework\TestCase;

final class SecondTest extends TestCase
{

    public function test1(): void
    {
        $this->assertSame(1, 1);
    }


    public function test2(): void
    {
        $this->assertSame(5, 5);
    }
}
